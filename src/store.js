import Vue from 'vue'
import Vuex from 'vuex'
import find from 'lodash/find'

Vue.use(Vuex)

function getDefaultTasks() {
    try {
        return JSON.parse(localStorage.tasks)
    } catch (e) {
        return []
    }
}

const store = new Vuex.Store({
    state: {
        tasks: getDefaultTasks(),
    },
    mutations: {
        addTask: (state, task) => state.tasks = [...state.tasks.filter(t => t.uuid !== task.uuid), task],
        deleteTask: (state, task) => state.tasks = state.tasks.filter(t => t.uuid !== task),
    },
    getters: {
        getTask: ({tasks}) => uuid => find(tasks, {uuid}),
    },
    actions: {},
})

setInterval(() => localStorage.tasks = JSON.stringify(store.state.tasks), 5000)


export default store
