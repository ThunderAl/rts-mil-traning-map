import Vue from 'vue'
import {directive} from './components/DragHelper'

export const lti = (deg = 0, min = 0, sec = 0) => deg * 60 * 60 + min * 60 + sec

export const itl = (i) => ({
    deg: parseInt(i / (60 * 60)),
    min: parseInt((i - (parseInt(i / (60 * 60)) * 60 * 60)) / 60),
    sec: (i - parseInt(i / (60 * 60)) * 60 * 60 - parseInt((i - (parseInt(i / (60 * 60)) * 60 * 60)) / 60) * 60),
})

export const map_size = {w: 4000, h: 7210}

export const linar = (x1, x2, y1, y2) => (x) => ((x - x1) * (y2 - y1)) / (x2 - x1) + y1
export const linar_reverse = (y1, y2, x1, x2) => linar(x1, x2, y1, y2)

export function linar_diffuse(x11, x12, y11, y12, x21, x22, y21, y22, min = 0, max = 1) {
    const linar1 = linar(x11, x12, y11, y12)
    const linar2 = linar(x21, x22, y21, y22)
    return (x, diffuse_state = (max - min) / 2) =>
        linar(min, max, linar1(x), linar2(x))(diffuse_state)
}

export function linar_diffuse_reverse(x11, x12, y11, y12, x21, x22, y21, y22, min = 0, max = 1) {
    const linar1 = linar_reverse(x11, x12, y11, y12)
    const linar2 = linar_reverse(x21, x22, y21, y22)
    return (x, diffuse_state = (max - min) / 2) =>
        linar(min, max, linar1(x), linar2(x))(diffuse_state)
}


Vue.filter('round', i => Math.round(i))

Vue.directive('drag', directive)
