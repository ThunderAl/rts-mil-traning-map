import Vue from 'vue'
import Router from 'vue-router'

import MainPage from './pages/MainPage/MainPage'
import MapPage from './pages/MapPage/MapPage'


Vue.use(Router)

export default new Router({
    routes: [
        {path: '/', component: MainPage, name: 'index'},

        {path: '/map', component: MapPage, name: 'map'},
    ],
})
