import find from 'lodash/find'
import registry from './registry'

class IconFactory {
    registry = registry
    icons = this.registry.flatMap(group => group.icons)

    getIconByUuid(uuid) {
        return this.find({uuid})
    }

    find(args) {
        return find(this.icons, args)
    }
}

export default new IconFactory
