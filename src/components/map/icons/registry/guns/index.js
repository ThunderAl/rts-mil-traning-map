const req = require.context('.', false, /^.\/[\w-_\d]+(?<!index)\.js$/)

export default {
    uuid: '04e5af42-1fbd-4f16-849d-0758ca59bd6b',
    name: 'guns',
    display: 'Орудия',
    icons: req.keys().map(req).map(m => m.default),
}
