const req = require.context('.', false, /^.\/[\w-_\d]+(?<!index)\.js$/)

export default {
    uuid: 'c9d9729b-4562-46b8-bdf7-5bfb8e82a5b1',
    name: 'anti_panzer_guns',
    display: 'Противотанковые (ПТ) пушки',
    description: 'Обратите внимание – характерный знак «ласточкин хвост» и в РФ, и в США',
    icons: req.keys().map(req).map(m => m.default),
}
