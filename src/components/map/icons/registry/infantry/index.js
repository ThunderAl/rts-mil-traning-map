const req = require.context('.', false, /^.\/[\w-_\d]+(?<!index)\.js$/)

export default {
    uuid: '1e2ab837-43ae-439e-9ec6-ab6d8bb9e5ef',
    name: 'infantry',
    display: 'Пехота, стрелковое и гранатометное оружие',
    icons: req.keys().map(req).map(m => m.default),
}
