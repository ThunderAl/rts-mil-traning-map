const req = require.context('.', true, /^.\/[\w-_\d]+\/index\.js$/)

export const groups = req.keys().map(req).map(m => m.default)
export default groups
