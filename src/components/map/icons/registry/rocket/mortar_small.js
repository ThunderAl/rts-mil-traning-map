export default {
    uuid: '54abaa1f-667e-4796-addc-a8a35284654d',
    name: 'mortar_small',
    display: 'Минометы: малого и среднего калибра (82-мм, 107-мм, 120-мм)',
    icon: require('./mortar_small.png'),
}
