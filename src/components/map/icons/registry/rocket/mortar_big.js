export default {
    uuid: '487af455-cfc2-40e3-a78f-cf033ab85038',
    name: 'mortar_all',
    display: 'Минометы: крупного калибра (160-мм, 240-мм СМ «Тюльпан»)',
    icon: require('./mortar_big.png'),
}
