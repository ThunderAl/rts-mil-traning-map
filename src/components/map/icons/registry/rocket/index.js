const req = require.context('.', false, /^.\/[\w-_\d]+(?<!index)\.js$/)

export default {
    uuid: '90b92590-589e-4536-9f1f-fa5e570ee789',
    name: 'rocket',
    display: 'Ракетные войска и артиллерия',
    icons: req.keys().map(req).map(m => m.default),
}
