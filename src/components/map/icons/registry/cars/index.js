const req = require.context('.', false, /^.\/[\w-_\d]+(?<!index)\.js$/)

export default {
    uuid: '20f49eca-f356-4fba-962a-5d4570c2a5b1',
    name: 'cars',
    display: 'Бронетехника и транспортная техника',
    icons: req.keys().map(req).map(m => m.default),
}
