export default {
    uuid: '931a4572-d248-45f1-ada4-2ccc0d63c20c',
    name: 'ptrk_2',
    display: 'ПТРК съемно-возимый, установленный на БТР',
    icon: require('./ptrk_2.png'),
}
