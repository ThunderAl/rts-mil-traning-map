const req = require.context('.', false, /^.\/[\w-_\d]+(?<!index)\.js$/)

export default {
    uuid: 'a2cce588-076a-4273-9e85-c61a6f2fd529',
    name: 'anti_panzer',
    display: 'Противотанковые ракетные комплексы (ПТРК)',
    icons: req.keys().map(req).map(m => m.default),
}
