import Position from './Position'

export default class Cursor {

    show = false
    position = null
    ratio = null

    constructor(ratio) {
        this.ratio = ratio
    }

    moveTo(realX, realY) {
        this.position = Position.fromCoords(realX, realY)
        return this
    }

    get left() {
        return this.position.left * this.ratio.zoom + this.ratio.offset_left*this.ratio.zoom
    }

    get top() {
        return this.position.top * this.ratio.zoom + this.ratio.offset_top*this.ratio.zoom
    }
}
