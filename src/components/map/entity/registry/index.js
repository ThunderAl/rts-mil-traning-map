const req = require.context('.', true, /^.\/[\w-_\d]+\/[\w-_\d]+Entity\.vue$/)

export const entity = req.keys().map(req)
export default entity
