import Vue from 'vue'
import registry from './registry'

registry.forEach(e => Vue.component(e.name, e.default))

class IconFactory {
    registry = registry
}

export default new IconFactory
