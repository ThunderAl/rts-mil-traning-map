import {lti, linar_diffuse, linar_diffuse_reverse, map_size} from '../../util'

const map_dots = {
    lat: [
        [1000, 3416, lti(60, 45), lti(60, 49)],
        [836, 3436, lti(60, 45), lti(60, 49)],
        [218600.66840103635, 218991.9700552736],
    ],
    lng: [
        [240, 6710, lti(55, 7), lti(55, 1)],
        [370, 6765, lti(55, 7), lti(55, 1)],
        [198433.2426584235, 198035.00478039015],
    ],
    Y: [
        [218666.06708604324, 218948.00045808623, 57000, 62000],
        [218674.1564202659, 218957.71126553402, 57000, 62000],
    ],
    X: [
        [198387.64555016055, 198066.57493583785, 12000, 2000],
        [198392.7808439807, 198072.6276308348, 12000, 2000],
    ],
}

let lat = linar_diffuse(...map_dots.lat[0], ...map_dots.lat[1], 0, map_size.h)
let lng = linar_diffuse(...map_dots.lng[0], ...map_dots.lng[1], 0, map_size.w)
let latr = linar_diffuse_reverse(...map_dots.lat[0], ...map_dots.lat[1], ...map_dots.lng[2])
let lngr = linar_diffuse_reverse(...map_dots.lng[0], ...map_dots.lng[1], ...map_dots.lat[2])

let Y = linar_diffuse(...map_dots.Y[0], ...map_dots.Y[1], ...map_dots.lng[2])
let X = linar_diffuse(...map_dots.X[0], ...map_dots.X[1], ...map_dots.lat[2])

export default class Position {
    _type = 'position'
    lat
    lng
    rotate = 0

    constructor(lat, lng, rotate = 0) {
        this.lat = lat
        this.lng = lng
        this.setRotate(rotate)
    }

    static fromCoords(realX, realY) {
        return new Position(lat(realX, realY), lng(realY, realX))
    }

    static parse({lat, lng, rotate = 0}) {
        return new Position(lat, lng, rotate)
    }

    get Y() {
        return Y(this.lat, this.lng)
    }

    get X() {
        return X(this.lng, this.lat)
    }

    get left() {
        return latr(this.lat, this.lng)
    }

    get top() {
        return lngr(this.lng, this.lat)
    }

    addRotate(angle) {
        return this.setRotate(this.rotate + angle)
    }

    clone() {
        return Position.parse(this)
    }

    setRotate(angle = 0) {
        if (angle > 360)
            return this.setRotate(angle - 360)
        if (angle < 0)
            return this.setRotate(angle + 360)
        this.rotate = angle
        return this
    }
}
