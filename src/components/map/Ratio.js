const zoom_border = {
    max: 3,
    min: 0.15,
}

export default class Ratio {

    zoom = 1
    offset_top = 0
    offset_left = 0

    width(w) {
        return w * this.zoom
    }

    height(h) {
        return h * this.zoom
    }

    top(t = 0, offset = true) {
        if (offset)
            return (this.offset_top + t) * this.zoom
        else
            return t * this.zoom
    }

    left(l = 0, offset = true) {
        if (offset)
            return (this.offset_left + l) * this.zoom
        else
            return l * this.zoom
    }

    applayDrag(deltaX, deltaY) {
        deltaX = deltaX || 0
        deltaY = deltaY || 0
        this.offset_top = this.offset_top + deltaY * (1 / this.zoom)
        this.offset_left = this.offset_left + deltaX * (1 / this.zoom)
    }

    applyZoom(delta) {
        let zoom = this.zoom + delta

        if (zoom < zoom_border.min)
            zoom = zoom_border.min
        if (zoom > zoom_border.max)
            zoom = zoom_border.max

        this.zoom = zoom
    }
}
