import isObject from 'lodash/isObject'
import isFunction from 'lodash/isFunction'

export default class DragHelper {

    target = null
    callback = null
    drag_start_callback = null
    drag_end_callback = null

    constructor(target) {
        this.target = target
    }

    onStart(callback) {
        this.drag_start_callback = callback
    }

    onEnd(callback) {
        this.drag_end_callback = callback
    }

    onDrag(callback) {
        this.callback = callback
    }

    listen() {
        this.dragStart = e => {
            let prevent = false

            if (this.drag_start_callback) {
                const data = this.drag_start_callback(e)
                prevent = data && data.prevent
            }

            if (!prevent)
                document.addEventListener('mousemove', this.callback)
        }


        this.dragEnd = () => {
            if (this.drag_end_callback)
                this.drag_end_callback()

            document.removeEventListener('mousemove', this.callback)
        }

        this.target.addEventListener('mousedown', this.dragStart)

        this.target.addEventListener('mouseup', this.dragEnd)
        document.addEventListener('mouseleave', this.dragEnd)
    }

    remove() {
        this.target.removeEventListener('mousedown', this.dragStart)

        this.target.removeEventListener('mouseup', this.dragEnd)
        this.target.removeEventListener('mouseleave', this.dragEnd)
    }

}

export const directive = {
    bind(node, {value}, v) {
        const drag = new DragHelper(node)

        if (isFunction(value)) {
            drag.callback = value
        } else if (isObject(value)) {
            drag.callback = value.callback
            drag.drag_start_callback = value.start
            drag.drag_end_callback = value.end
        }

        drag.listen()
        v.$data = v.$data || {}
        v.$data.$dragHelper = drag
    },
}
