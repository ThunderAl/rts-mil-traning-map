import isObject from 'lodash/isObject'
import isArray from 'lodash/isArray'
import Position from './map/Position'

const types = {
    position: json => Position.parse(json),
}

export function deserialize(json) {
    if (!isObject(json) && !isArray(json)) return json

    if (isArray(json)) {
        return json.reduce((carry, element) => {
            return [
                ...carry,
                deserialize(element),
            ]
        }, [])
    }

    if (isObject(json)) {

        if (json._type && types[json._type])
            return types[json._type](json)

        return Object.keys(json).reduce((carry, key) => {
            return {
                ...carry,
                [key]: deserialize(json[key]),
            }
        }, {})
    }
}
